#include "NoOneToDie.h"
#include "customer_define.h"
#include "HelloWorldScene.h"

USING_NS_CC;

CCScene* NoOneToDie::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
	NoOneToDie *layer = NoOneToDie::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

cocos2d::CCScene* NoOneToDie::scene(int floor)
{
	CCScene *scene = CCScene::create();
	NoOneToDie *layer = NoOneToDie::create(floor);
	scene->addChild(layer);
	return scene;
}


NoOneToDie* NoOneToDie::create(int floor)
{
	NoOneToDie *pRet = new NoOneToDie();
	//此处为进行出错检查
	pRet->m_Floor = floor;
	if (pRet && pRet->init()) {
		pRet->autorelease();
	}
	else {
		delete pRet;
		pRet = NULL;
	}
	return pRet;
}

// on "init" you need to initialize your instance
bool NoOneToDie::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
	StartGame = false;	//先别开启游戏
	srand(time(NULL));	//初始化随机数种子
	m_TotalTime = 0;	//初始化时间
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
	pLabelTime = CCLabelTTF::create("0.00 S" , "Arial" , 24);
    
    // position the label on the center of the screen
	pLabelTime->setPosition(ccp(origin.x + visibleSize.width / 2 ,
								origin.y + visibleSize.height - pLabelTime->getContentSize().height));

    // add the label as a child to this layer
	pLabelTime->retain();
	this->addChild(pLabelTime , 15);
	//绘制白色的背景
	//CCLayerColor *LayerWhiteBack = CCLayerColor::create(ccc4(255 , 255 , 255 , 255) , WinWidth , WinHeigth);
	//LayerWhiteBack->setAnchorPoint(CCPointZero);
	//LayerWhiteBack->setPosition(ccp(0 , 0));
	//addChild(LayerWhiteBack,-1);
	//添加人物
	for (int i = 0; i < m_Floor; ++i) {
		hero[i] = Hero::create();
		hero[i]->setAnchorPoint(CCPointZero);
		hero[i]->setPosition(ccp(10 , i*(WinHeigth / m_Floor) + 4));
		addChild(hero[i]);
	}
	//初始化障碍块
	for (int i = 0; i < m_Floor * 3; ++i) {
		Block[i] = CCSprite::create("blackBlock.png" , CCRectMake(0,0,15 , WinHeigth / 10));
		//Block[i]->setContentSize(CCSizeMake(15 , WinHeigth / m_Floor / 2));	//设置一层高度的一半
		//CCLog("w=  %g    h=  %g" , Block[i]->boundingBox().size.width , Block[i]->boundingBox().size.height);
		/*Block[i]->setScaleX(1.5f);
		Block[i]->setScaleY(0.7f);*/

		//Block[i]->setContentSize(CCSizeMake(20 , 30));
		//CCLog("w=  %g    h=  %g" , Block[i]->boundingBox().size.width , Block[i]->boundingBox().size.height);
		Block[i]->setAnchorPoint(CCPointZero);	//设置锚点
		Block[i]->setPosition(ccp(i * WinWidth+rand()%200+300 , i / 3 * (WinHeigth / m_Floor) + 4));
		addChild(Block[i],4);
		
	}
	//开启游戏
	StartGame = true;
	schedule(schedule_selector(NoOneToDie::TotalUpdate) , 0.1f);
	//设置触摸和单点触摸
	setTouchEnabled(true);
	setTouchMode(kCCTouchesOneByOne);
    return true;
}
///////////////////////////////////////////////////////////////////////////
//完成地板的绘制已经碰撞检测
void NoOneToDie::draw()
{	


//==============绘制地板部分===============================
	float horizonHeight = 0.0f;
	//绘制地板
	//ccDrawColor4B(16 , 32 , 48 , 255);
	ccDrawColor4B(230 , 180 , 255 , 255);
	glLineWidth(5);
	for (int horizon = 0; horizon < m_Floor; ++horizon) {
	//	CCLog("Draw m_Floor = %d" , m_Floor);
		horizonHeight = horizon*(WinHeigth / m_Floor);
		ccDrawLine(ccp(0 , horizonHeight+5) , ccp(WinWidth , horizonHeight+5));
	}




//还是放在这里面做，比较好一点
//===============顺便做一做碰撞检测，和障碍块的变换=====================
	int width = (int)WinWidth;
	float ScaX;
	float ScaY;
	if (StartGame) {
		for (int i = 0; i < m_Floor * 3; ++i) {
			//向左边位移
			Block[i]->setPositionX(Block[i]->getPositionX() - 6);
			//进行碰撞检测
			//如果人物的x在障碍的x-hero.width到障碍的x+block.width之间，且高度不足，发生碰撞
			for (int j = 0; j < m_Floor; ++j) {
				if ((hero[j]->getPositionX() + hero[j]->boundingBox().size.width - 5) > Block[i]->getPositionX() &&
					hero[j]->getPositionX() < (Block[i]->getPositionX() + Block[i]->boundingBox().size.width - 5) &&
					hero[j]->getPositionY() < Block[i]->boundingBox().size.height) {
					StartGame = false;
					{ /*CCLog("peng zhuang ");
						CCLog("w=    %g    h=    %g" , Block[i]->boundingBox().size.width , Block[i]->boundingBox().size.height);
						CCLog("hx=    %g    y=    %g" , hero[j]->getPositionX() , hero[j]->getPositionY());
						CCLog("bx=    %g    y=    %g" , Block[i]->getPositionX() , Block[i]->getPositionY());*/ }
				}
			}

			//移动到了屏幕之外后返回最后再来,并重新设置缩放大小
			if (Block[i]->getPositionX()<-20) {
				//设置位置为其前面一个位置加上[100,winwidth+150]
				Block[i]->setPositionX(Block[(i+2)%3]->getPositionX() + rand() % width + 150);
				ScaX = 0.1f*(rand() % 7) + 0.7;	//[0.7,1.4]
				ScaY = 0.1f*(rand() % 8) + 0.5;	//[0.8,1.3]
				Block[i]->setScaleX(ScaX);
				Block[i]->setScaleY(ScaY);
				//CCLog("w=    %g    h=    %g" , Block[i]->boundingBox().size.width , Block[i]->boundingBox().size.height);
				//CCLog("ScaX= %g    ScaY= %g" , ScaX , ScaY);
			}

		}

	}

}

////////////////////////////////////////////////////////////////////////////
//时间累积
void NoOneToDie::TotalUpdate(float dt)
{
	if (StartGame) {
		char time[16] = { 0 };
		m_TotalTime += dt;
		sprintf(time , "%0.2f S" , m_TotalTime);
		//CCLog("time %s" , time);
		pLabelTime->setString(time);
	}
	else {
		//处理游戏失败事件
		CCLayerColor *layer = CCLayerColor::create(ccc4(0 , 36 , 120 , 255) , WinWidth , WinHeigth);
		layer->setAnchorPoint(CCPointZero);
		addChild(layer , 10);
		layer->setPosition(CCPointZero);
		CCMenuItemFont * itemfont = CCMenuItemFont::create("Back Main Menu" , this , menu_selector(NoOneToDie::BackMainMenu));
		CCMenu * menu = CCMenu::create(itemfont , NULL);
		layer->addChild(menu);
		unschedule(schedule_selector(NoOneToDie::TotalUpdate));
	}
}

////////////////////////////////////////////////////////////////////////////
//触摸点击事件，人物跳跃
bool NoOneToDie:: ccTouchBegan(CCTouch *pTouch , CCEvent *pEvent)
{
	int i = (int)pTouch->getLocation().y;
	CCLog("i=   %d" , i);

	i /= WinHeigth / m_Floor;
	CCLog("i=   %d" , i);
	if (hero[i]->getPositionY() < i*(WinHeigth / m_Floor) + 10) {
		CCJumpBy *jump = CCJumpBy::create(0.4 , ccp(15 , 0) , WinHeigth / 5 , 1);
		CCMoveBy *move = CCMoveBy::create(0.2 , ccp(-15 , 0));
		hero[i]->runAction(jump);
		hero[i]->runAction(move);
	}
	return false;
}

void NoOneToDie::BackMainMenu(CCObject* obj)
{	//替换场景
	CCScene * scene = HelloWorld::scene();
	CCDirector::sharedDirector()->replaceScene(scene);
}

//void HelloWorld::menuCloseCallback(CCObject* pSender)
//{
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT) || (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
//	CCMessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
//#else
//    CCDirector::sharedDirector()->end();
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//    exit(0);
//#endif
//#endif
//}
