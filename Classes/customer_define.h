#ifndef __CUSTOMER_DEFINE_H__
#define __CUSTOMER_DEFINE_H__
#include "cocos2d.h"
USING_NS_CC;
/**
* define a scene function for a specific type, such as CCLayer
* @__TYPE__ class type to add scene(), such as CCLayer
*/
#define SCENE_FUNC(__TYPE__) \
static cocos2d::CCScene* scene() \
{ \
	cocos2d::CCScene* scene = cocos2d::CCScene::create(); \
	__TYPE__ * sprite = __TYPE__::create(); \
	scene->addChild(sprite); \
	return scene; \
}


#define WinSize (cocos2d::CCDirector::sharedDirector()->getWinSize())

#define WinWidth  (cocos2d::CCDirector::sharedDirector()->getWinSize().width)

#define WinHeigth  (cocos2d::CCDirector::sharedDirector()->getWinSize().height)

//头文件中定义函数，在多处包含，造成重定义
//float getWINHEIGH()
//{
//	return cocos2d::CCDirector::sharedDirector()->getWinSize().height;
//}

#endif __CUSTOMER_DEFINE_H__