#ifndef __NOONETODIE_H__
#define __NOONETODIE_H__

#include "cocos2d.h"
#include "Hero.h"


class NoOneToDie : public cocos2d::CCLayer
{
public:
	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::CCScene* scene();
	static cocos2d::CCScene* scene(int floor);	//选关进入

	// a selector callback


	// implement the "static node()" method manually
	CREATE_FUNC(NoOneToDie);
	static NoOneToDie* create(int floor);	//选关进入
private:
	void draw();	//绘制函数，绘制楼层地板
	bool ccTouchBegan(CCTouch *pTouches , CCEvent *pEvent);
	void TotalUpdate(float dt);	//时间更新函数
	void BackMainMenu(CCObject* obj);

private:
	//int m_Floor = 1;	//有几层，游戏难度控制
	int m_Floor ;	//有几层，游戏难度控制
	float m_TotalTime;	//时间累计，已经坚持了多久s
	Hero* hero[3];	//保存人物，最多3个
	CCSprite* Block[9];	//障碍块，每层最多三个
	bool StartGame;		//游戏是否开始的标识
	CCLabelTTF* pLabelTime;	//时间显示
};

#endif // __NOONETODIE_H__
