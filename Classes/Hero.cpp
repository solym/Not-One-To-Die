#include "Hero.h"


bool Hero::init()
{
	if (!CCSprite::init()) {
		return false;
	}
	//=========================================================================
	//加载人物大图
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("hero.plist");
	//创建人物动画
	CCAnimation *hero = CCAnimation::create();
	char heroName[16] = { 0 };
	for (int i = 1; i < 6; ++i) {
		sprintf(heroName , "Hero%d.png" , i);
		//CCLog("NAME %s" , heroName);
		hero->addSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(heroName));
	}
	hero->setDelayPerUnit(0.1f);	//设间隔
	hero->setLoops(UINT_MAX);	//	设置循环次数为最大

	//创建动画动作   animate  adj.有生命的；有活力的；有生气的 v.使具活力；使生气勃勃；把…制作成动画片 
	CCAnimate * ActionHero = CCAnimate::create(hero);
	//运行动作
	this->runAction(ActionHero);
	return true;
}
