#include "HelloWorldScene.h"
#include "customer_define.h"
#include "NoOneToDie.h"


USING_NS_CC;

CCScene* HelloWorld::scene()
{
	// 'scene' is an autorelease object
	CCScene *scene = CCScene::create();

	// 'layer' is an autorelease object
	HelloWorld *layer = HelloWorld::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	//////////////////////////////
	// 1. super init first
	if (!CCLayer::init()) {
		return false;
	}

	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	/////////////////////////////
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//    you may modify it.

	// add a "close" icon to exit the progress. it's an autorelease object
	CCMenuItemImage *pCloseItem = CCMenuItemImage::create(
		"CloseNormal.png" ,
		"CloseSelected.png" ,
		this ,
		menu_selector(HelloWorld::menuCloseCallback));

	//pCloseItem->setPosition(ccp(origin.x + visibleSize.width - pCloseItem->getContentSize().width/2 ,
	//                            origin.y + pCloseItem->getContentSize().height/2));


	//添加  简单、中等、较难
	CCMenuItemImage * easy = CCMenuItemImage::create("easy.png" , "easy.png" , this , menu_selector(HelloWorld::menuEasyCallBack));
	CCMenuItemImage * medium = CCMenuItemImage::create("medium.png" , "medium.png" , this , menu_selector(HelloWorld::menuMediumCallBack));
	CCMenuItemImage * hard = CCMenuItemImage::create("hard.png" , "hard.png" , this , menu_selector(HelloWorld::menuHardCallBack));

	// create menu, it's an autorelease object
	CCMenu* pMenu = CCMenu::create(pCloseItem , NULL);

	easy->setAnchorPoint(ccp(0.5 , 0.5));
	//easy->setPosition(ccp(240,160));
	pMenu->addChild(easy);
	pMenu->addChild(medium);
	pMenu->addChild(hard);
	pMenu->alignItemsVertically();
	//pMenu->setPosition(CCPointZero);
	pCloseItem->setAnchorPoint(ccp(0.5 , 0.5));
	pCloseItem->setPosition(ccp(WinWidth / 2 - 30 , 30 - WinHeigth / 2));

	this->addChild(pMenu , 5);

	/////////////////////////////
	// 3. add your codes below...

	// add a label shows "Hello World"
	// create and initialize a label

	//CCLabelTTF* pLabel = CCLabelTTF::create("Hello World", "Arial", 24);

	// position the label on the center of the screen
	//pLabel->setPosition(ccp(origin.x + visibleSize.width/2,
	//                        origin.y + visibleSize.height - pLabel->getContentSize().height));

	// add the label as a child to this layer
	//this->addChild(pLabel, 1);

	// add "HelloWorld" splash screen"
	//CCSprite* pSprite = CCSprite::create("HelloWorld.png");

	// position the sprite on the center of the screen
	//pSprite->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

	// add the sprite as a child to this layer
	//this->addChild(pSprite, 0);

	return true;
}


void HelloWorld::menuCloseCallback(CCObject* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT) || (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
	CCMessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
#else
	//CCScene *ps2 = StartGame::scene();
	//CCDirector* pDirector = CCDirector::sharedDirector();
	//pDirector->replaceScene(ps2);
	//Sleep(3000);
	//pDirector->replaceScene((CCScene*)getChildByTag(123));
	//pDirector->replaceScene(NULL);
	//导演宣布结束
	CCDirector::sharedDirector()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
#endif
}


void HelloWorld::menuEasyCallBack(CCObject* pSender)
{
	//菜单点击的时候跳到  StartGame::scene()
	CCScene *ps2 = NoOneToDie::scene(1);
	CCDirector* pDirector = CCDirector::sharedDirector();
	pDirector->replaceScene(ps2);

}

void HelloWorld::menuMediumCallBack(CCObject* pSender)
{
	//菜单点击的时候跳到  StartGame::scene()
	CCScene *ps2 = NoOneToDie::scene(2);
	CCDirector* pDirector = CCDirector::sharedDirector();
	pDirector->replaceScene(ps2);

}

void HelloWorld::menuHardCallBack(CCObject* pSender)
{
	//菜单点击的时候跳到  StartGame::scene()
	CCScene *ps2 = NoOneToDie::scene(3);
	CCDirector* pDirector = CCDirector::sharedDirector();
	pDirector->replaceScene(ps2);

}
